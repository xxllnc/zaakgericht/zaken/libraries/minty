# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import io
import zipfile
from collections import namedtuple
from minty.infrastructure.mime_utils import get_mime_type_from_handle, is_zip
from unittest import mock


class TestMimeUtils:
    def test_is_zip(self):
        file_handle = io.BytesIO(b"PK\x03\x04")
        assert is_zip(file_handle)
        file_handle = io.BytesIO(b"fake")
        assert not is_zip(file_handle)

    @mock.patch("magic.detect_from_content", autospec=True)
    def test_get_mime_type_from_content(self, mock_detect_content):
        file_handle = io.BytesIO(b"PK\x03\x04")

        MimeType = namedtuple("MimeType", "mime_type")
        mock_detect_content.return_value = MimeType(mime_type="mock/content")

        assert get_mime_type_from_handle(file_handle) == "mock/content"

    @mock.patch("minty.infrastructure.mime_utils.get_mime_type_from_content")
    @mock.patch("minty.infrastructure.mime_utils.is_zip")
    @mock.patch("zipfile.ZipFile", autospec=True)
    def test_get_mime_type_from_handle(
        self, mock_zipfile, mock_is_zip, mock_get_from_content
    ):
        file_handle = io.BytesIO()

        zip_reader = mock.MagicMock()
        zip_reader.read = mock.MagicMock()
        zip_reader.read.return_value = """<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<Types xmlns="http://schemas.openxmlformats.org/package/2006/content-types">
    <Default Extension="png" ContentType="image/png" />
    <Default Extension="rels" ContentType="application/vnd.openxmlformats-package.relationships+xml" />
    <Default Extension="xml" ContentType="application/xml" />
    <Override PartName="/docProps/app.xml" ContentType="application/vnd.openxmlformats-officedocument.extended-properties+xml" />
    <Override PartName="/docProps/core.xml" ContentType="application/vnd.openxmlformats-package.core-properties+xml" />
    <Override PartName="/word/document.xml" ContentType="application/vnd.openxmlformats-officedocument.wordprocessingml.document.main+xml" />
    <Override PartName="/word/fontTable.xml" ContentType="application/vnd.openxmlformats-officedocument.wordprocessingml.fontTable+xml" />
    <Override PartName="/word/footer1.xml" ContentType="application/vnd.openxmlformats-officedocument.wordprocessingml.footer+xml" />
    <Override PartName="/word/footer2.xml" ContentType="application/vnd.openxmlformats-officedocument.wordprocessingml.footer+xml" />
    <Override PartName="/word/footer3.xml" ContentType="application/vnd.openxmlformats-officedocument.wordprocessingml.footer+xml" />
    <Override PartName="/word/header1.xml" ContentType="application/vnd.openxmlformats-officedocument.wordprocessingml.header+xml" />
    <Override PartName="/word/header2.xml" ContentType="application/vnd.openxmlformats-officedocument.wordprocessingml.header+xml" />
    <Override PartName="/word/header3.xml" ContentType="application/vnd.openxmlformats-officedocument.wordprocessingml.header+xml" />
    <Override PartName="/word/settings.xml" ContentType="application/vnd.openxmlformats-officedocument.wordprocessingml.settings+xml" />
    <Override PartName="/word/styles.xml" ContentType="application/vnd.openxmlformats-officedocument.wordprocessingml.styles+xml" />
    <Override PartName="/word/theme/theme1.xml" ContentType="application/vnd.openxmlformats-officedocument.theme+xml" />
    <Override PartName="/word/webSettings.xml" ContentType="application/vnd.openxmlformats-officedocument.wordprocessingml.webSettings+xml" />
</Types>
        """
        mock_zipfile.return_value = zip_reader
        mock_is_zip.return_value = True

        assert (
            get_mime_type_from_handle(file_handle)
            == "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
        )

        zip_reader.read.return_value = ""
        mock_zipfile.return_value = zip_reader

        mock_is_zip.return_value = False
        mock_get_from_content.return_value = "mock/fobj"
        assert get_mime_type_from_handle(file_handle) == "mock/fobj"

        mock_is_zip.side_effect = zipfile.BadZipFile
        assert get_mime_type_from_handle(file_handle) == "mock/fobj"
