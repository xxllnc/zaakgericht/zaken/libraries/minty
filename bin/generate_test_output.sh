#!/bin/bash

# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../"

mkdir -p /root/test_result
pytest --junitxml=/root/test_result/junit.xml --cov=minty --cov-report=term --cov-branch
